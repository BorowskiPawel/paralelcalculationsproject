﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace OpenMP
{
    class Program
    {
        static void Main(string[] args)
        {
            var iterationsValues = new long[]
            {
                //10000000,
                1000000000
            };

            var paraleismValues = new int[]
            {
                //1,
                10,
                8,
                6,
                4,
                2
            };

            foreach (var iteration in iterationsValues)
            {
                foreach (var paraleism in paraleismValues)
                {
                    Console.WriteLine($"----------------------------");
                    Console.WriteLine($"Iterations: {iteration}");
                    Console.WriteLine($"Threads: {paraleism}");
                    calculate(iteration, paraleism);
                }
            }
        }

        static void calculate(long iterations, int maxDegreeOfParallelism)
        {
            var partialResults = new decimal[iterations - 1];
            
            var sw = Stopwatch.StartNew();

            Parallel.For(1, iterations, new ParallelOptions {MaxDegreeOfParallelism = maxDegreeOfParallelism}, (i, state) =>
            {
                decimal localResult = 2 * i;
                partialResults[i - 1] = (localResult / (localResult - 1)) * (localResult / (localResult + 1));
            });

            decimal result = 1;
            for (var i = 0; i < iterations - 1; i++)
            {
                result *= partialResults[i];
            }
            
            Console.WriteLine($"Time elapsed: {sw.Elapsed}");
            Console.WriteLine($"Pi: {2*result}");
        }
    }
}