#include <mpi.h>
#include <array>
#include <functional>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>
#include <chrono>

using namespace std;

long double calculate(const long long startIndex, const long long count)
{
  cout << "Calculation |  Index: " << startIndex << "  Count: " << count << endl;
  long double result = 1.0;
  for (long long index = startIndex; index < startIndex + count; index++)
  {
    long double temp = 2.0 * index;
    result *= (temp / (temp - 1.0)) * (temp / (temp + 1.0));
  }
  return result;
}

int main(int argc, char *argv[])
{
  long long i = 1;
  long long iterations = 100000000;

  MPI_Init(nullptr, nullptr);
  auto startTime = chrono::system_clock::now();
  int worldSize, workerId;
  MPI_Comm_size(MPI_COMM_WORLD, &worldSize);
  MPI_Comm_rank(MPI_COMM_WORLD, &workerId);

  MPI_Barrier(MPI_COMM_WORLD);

  if (workerId == 0)
  {
    for (int workerIndex = 1; workerIndex < worldSize; workerIndex++)
    {
      long long startIndex = ((iterations - i) * workerIndex / worldSize) + i;
      long long count = (iterations - i) / worldSize;
      array<long long, 2> batchInfo = {startIndex, count};
      MPI_Send(&batchInfo, 2, MPI_LONG_LONG, workerIndex, 0, MPI_COMM_WORLD);
    }

    vector<long double> computationResults;
    long double firstPartialResult = calculate(((iterations - i) * 0 / worldSize) + i, (iterations - i) / worldSize);
    computationResults.push_back(firstPartialResult);
    cout << "WorkerId: " << 0 << " startIndex: " << 1 << " count: " << (iterations - i) / worldSize << endl;

    for (int workerIndex = 1; workerIndex < worldSize; workerIndex++)
    {
      long double partialResult;

      //cout << "Waiting for workerId: " << workerIndex << endl;
      MPI_Recv(&partialResult, 1, MPI_LONG_DOUBLE, workerIndex, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
      computationResults.push_back(partialResult);
    }

    long double result = accumulate(computationResults.begin(), computationResults.end(), 1.0, multiplies<long double>());

    cout << endl << "Pi: " << setprecision(150) << result * 2 << endl;
  }

  else
  {
    array<long long, 2> batchInfo;
    MPI_Recv(&batchInfo, 2, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    long double partialResult = calculate(batchInfo[0], batchInfo[1]);
    cout << "WorkerId: " << workerId << " startIndex: " << batchInfo[0] << " count: " << batchInfo[1] << endl;

    MPI_Send(&partialResult, 1, MPI_LONG_DOUBLE, 0, 0, MPI_COMM_WORLD);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  auto endTime = chrono::system_clock::now();
  chrono::duration<double> elapsedTime = endTime - startTime;
  if (workerId == 0)
  {
    cout << "Execution time:  " << setprecision(3) << elapsedTime.count() << " seconds. Executed  with " << worldSize << " workers." << endl;
  }
  
  MPI_Finalize();
  return 0;
}
