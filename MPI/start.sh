#!/bin/bash
#
#SBATCH --job-name=mpi_job
#SBATCH --output=mpi-results/mpi.txt
#
#SBATCH --nodes=4

#SBATCH --ntasks-per-node=1
#SBATCH --time=10:00

mpirun ./a.out